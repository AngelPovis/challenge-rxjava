package org.globant.challenge.service;

import io.reactivex.Completable;
import io.reactivex.Single;
import lombok.RequiredArgsConstructor;
import org.globant.challenge.domain.Status;
import org.globant.challenge.domain.Student;
import org.globant.challenge.dto.student.CreateStudent;
import org.globant.challenge.dto.student.UpdateStudent;
import org.globant.challenge.error.StudentNotFoundException;
import org.globant.challenge.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public Single<Student> addStudent(CreateStudent student) {
        return Single.create(singleSubscriber -> singleSubscriber.onSuccess(this.studentRepository.save(Student.getFromCreation(student))));
    }

    @Override
    public Single<Student> getStudent(Long id) {
        return Single.create(singleSubscriber -> this.studentRepository.findByIdAndStatus(id, Status.ACTIVE).ifPresentOrElse(singleSubscriber::onSuccess, () -> singleSubscriber.onError(new StudentNotFoundException())
        ));
    }

    @Override
    public Single<List<Student>> getActiveStudents() {
        return Single.create(singleSubscriber -> {
                    List<Student> students = this.studentRepository.findAllByStatus(Status.ACTIVE);
                    if (students.isEmpty()) {
                        singleSubscriber.onError(new StudentNotFoundException());
                    } else {
                        singleSubscriber.onSuccess(students);
                    }
                }
        );

    }

    @Override
    public Completable deleteStudent(Long id) {
        return Completable.create(completableSubscriber -> this.studentRepository.findById(id).ifPresentOrElse(student -> {
                    student.setStatus(Status.INACTIVE);
                    this.studentRepository.save(student);
                    completableSubscriber.onComplete();
                },
                () -> completableSubscriber.onError(new StudentNotFoundException())
        ));
    }

    @Override
    public Completable updateStudent(UpdateStudent newStudent) {
        return Completable.create(completableSubscriber -> this.studentRepository.findById(newStudent.getId()).ifPresentOrElse(
                student -> {
                    student.setFirstName(newStudent.getFirstName());
                    student.setLastName(newStudent.getLastName());
                    this.studentRepository.save(student);
                    completableSubscriber.onComplete();
                }
                ,
                () -> completableSubscriber.onError(new StudentNotFoundException())));
    }

}
