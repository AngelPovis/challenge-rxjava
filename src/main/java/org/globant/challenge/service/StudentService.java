package org.globant.challenge.service;

import io.reactivex.Completable;
import io.reactivex.Single;
import org.globant.challenge.domain.Student;
import org.globant.challenge.dto.student.CreateStudent;
import org.globant.challenge.dto.student.UpdateStudent;

import java.util.List;

public interface StudentService {
    Single<Student> addStudent(CreateStudent student);

    Single<Student> getStudent(Long id);

    Single<List<Student>> getActiveStudents();

    Completable deleteStudent(Long id);

    Completable updateStudent(UpdateStudent student);
}
