package org.globant.challenge.repository;

import org.globant.challenge.domain.Status;
import org.globant.challenge.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    List<Student> findAllByStatus(@Param("status") Status status);

    Optional<Student> findByIdAndStatus(@Param("id") Long id, @Param("status") Status active);
}
