package org.globant.challenge.error;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class StudentNotFoundException extends RuntimeException {

    private final HttpStatus httpStatus = HttpStatus.NOT_FOUND;

    public StudentNotFoundException() {
        super("Student not found");
    }
}
