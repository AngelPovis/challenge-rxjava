package org.globant.challenge.domain;

public enum Status {
    ACTIVE, INACTIVE;
}
