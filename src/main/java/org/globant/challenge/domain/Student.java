package org.globant.challenge.domain;

import lombok.Getter;
import lombok.Setter;
import org.globant.challenge.dto.student.CreateStudent;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;
    @Column(name = "first_name")
    String firstName;
    @Column(name = "last_name")
    String lastName;
    Status status;

    public static Student getFromCreation(CreateStudent createStudent) {
        Student student = new Student();
        student.setStatus(Status.ACTIVE);
        student.setFirstName(createStudent.getFirstName());
        student.setLastName(createStudent.getLastName());
        return student;
    }
}
