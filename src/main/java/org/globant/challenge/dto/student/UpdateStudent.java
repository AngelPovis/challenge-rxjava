package org.globant.challenge.dto.student;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UpdateStudent {
    @NotNull
    @Min(1L)
    private Long id;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
}
