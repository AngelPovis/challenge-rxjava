package org.globant.challenge.dto.student;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class CreateStudent {
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
}
