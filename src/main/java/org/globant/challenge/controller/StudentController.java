package org.globant.challenge.controller;

import io.reactivex.Completable;
import io.reactivex.Single;
import lombok.RequiredArgsConstructor;
import org.globant.challenge.domain.Student;
import org.globant.challenge.dto.student.CreateStudent;
import org.globant.challenge.dto.student.UpdateStudent;
import org.globant.challenge.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<Student>> addStudent(@RequestBody @Validated CreateStudent student) {
        return studentService.addStudent(student).map(ResponseEntity::ok);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<Student>> getStudent(@PathVariable("id") Long id) {
        return studentService.getStudent(id).map(ResponseEntity::ok);
    }

    @GetMapping(value = "/active", produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<List<Student>>> getActiveStudent() {
        return studentService.getActiveStudents().map(ResponseEntity::ok);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Completable updateStudent(@RequestBody @Validated UpdateStudent student) {
        return studentService.updateStudent(student);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Completable deleteStudent(@PathVariable("id") Long id) {
        return studentService.deleteStudent(id);
    }

}
