package org.globant.challenge.controller;

import org.globant.challenge.error.StudentNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AdviceController {

    @ExceptionHandler(StudentNotFoundException.class)
    public ResponseEntity<String> handleEntityNotFoundException(StudentNotFoundException e) {
        return new ResponseEntity<>(e.getMessage(), e.getHttpStatus());
    }
}
